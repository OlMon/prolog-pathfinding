%Knowledge-Base
next_to(main_office, mail).
next_to(mail, ts).
next_to(ts, stairs).
next_to(ts, o101).
next_to(o101, r101).
next_to(o101, o103).
next_to(o103, r103).
next_to(o103, o105).
next_to(o105, r105).
next_to(o105, o107).
next_to(o107, r107).
next_to(o107, o109).
next_to(o109, r109).
next_to(o109, o111).
next_to(0109, o113).
next_to(o111, r111).
next_to(o113, r113).

%Reverting the next_to rules
way(X,Y) :-
    next_to(X,Y);
    next_to(Y,X).

%Reverting the next_to rules (without 'sugar')
way_ws(X,Y) :- next_to(X,Y).
way_ws(X,Y) :- next_to(Y,X).

%Alternative Knowledge-Base (not used)
ways(mail, [main_office,ts]).
ways(ts,[o101,stairs,mail]).
ways(stairs,[ts]).
ways(o101,[r101,o103,ts]).
ways(r101,[o101]).
ways(o103,[o105,o101]).
ways(o105,[r105,o103]).

%Declaration of closed doors with the key location
closed(o101, r101, main_office).
closed(o103, r103, r101).
closed(o105, r105, r103).
closed(o107, r107, r105).
closed(o109, r109, r107).
closed(o111, r111, r109).
closed(o103, r103, r111).

%List concat
append_to([],L,L). 
append_to([H|T],L2,[H|L3]) :-
    append_to(T,L2,L3).

%Reverting a list
rev_list([], []).
rev_list([H|T], Reversed) :-
    rev_list(T, ReversedRest),
    append_to(ReversedRest, [H], Reversed).
    
%Check if element in list
already_visited(X, [X|_]).
already_visited(X,[_|T]) :-
    already_visited(X,T).

%Simple depth-first search
solve_dfs(Start,Finish,Path) :-
    dfs(Start,Finish,[Start],Path).
dfs(Node, Node, _, [Node]).
dfs(Start,Finish, Visited, [Start|Path]) :-
    way(Start, Next),
    ((closed(Start,Next,Key))->
         already_visited(Key, Visited);true),
    dfs(Next, Finish, [Next | Visited], Path).

%Depth-first search with limited depth
solve_dfs_wld(Start,Finish,Depth,Path) :-
    dfs_wld(Start,Finish,[Start],Depth,Path).
dfs_wld(Node, Node, _, _, [Node]).
dfs_wld(Start,Finish, Visited,Depth,[Start|Path]) :-
    way(Start, Next),
    Depth >= 0,
    ((closed(Start,Next,Key))->
         already_visited(Key, Visited);true),
    dfs_wld(Next, Finish, [Next | Visited],Depth-1, Path).

%Check if the way is closed and if the robot has already the key 
pass_through(Start, Next, Visited) :-
    closed(Start, Next, Key), !, already_visited(Key, Visited).
pass_through(_,_,_).

%Depth-first search with limited depth (without 'sugar')
solve_dfs_wld_ws(Start,Finish,Depth,Path) :-
    dfs_wld_ws(Start,Finish,[Start],Depth,Path).
dfs_wld_ws(Node, Node, _, _, [Node]).
dfs_wld_ws(Start,Finish, Visited,Depth,[Start|Path]) :-
    way_ws(Start, Next),
    Depth >= 0,
    pass_through(Start, Next, Visited),
    dfs_wld_ws(Next, Finish, [Next | Visited],Depth-1, Path).


%Iterative deepening
solve_id(Start, Finish, Path) :-
    solve_id(Start, Finish, 1, Path).
solve_id(Start, Finish, Depth, Path) :-
    solve_dfs_wld(Start, Finish, Depth, Path);
    solve_id(Start, Finish, Depth + 1, Path).

%Breadth-first search with result in reversed order
solve_bfs_r(Start, Finish, Solution) :-
    bfs_r([[Start]], Finish, Solution).
bfs_r([[Finish|Path]|_], Finish, [Finish|Path]).
bfs_r([Path|Paths], Finish, Solution) :-
    extend(Path, NewPaths),
    append_to(Paths, NewPaths, AllPaths),
    bfs_r(AllPaths, Finish, Solution).

%Breadth-first search with result in right order
solve_bfs(Start, Finish, Solution) :-
    bfs([[Start]], Finish, _, Solution).
bfs([[Finish|Path]|_], Finish, [Finish|Path], Solution) :-
    rev_list([Finish|Path], Solution).
bfs([Path|Paths], Finish, Frontier, Solution) :-
    extend(Path, NewPaths),
    append_to(Paths, NewPaths, AllPaths),
    bfs(AllPaths, Finish, Frontier, Solution).

%Helppredicate for finding all possible ways from a point
extend([Node| Path], NewPaths) :-
    findall([NewNode, Node|Path], (way(Node, NewNode), ((closed(Node,NewNode,K))->already_visited(K, Path);true)), NewPaths).

